## ChIP-Seq detects ANT genome wide binding sites in stage 6/7 flowers

ANT is a transcription factor in Arabidopsis that regulates floral organ development. This experiment aims to answer the question:

* Which genes and pathways are directly regulated by ANT?

### Experimental design

Tagged ANT under the control of its endogenous promoter and in the ant mutant background was introduced into a synchronized floral induction system such that flowers of a single stage could be collected. Inflorescences of tagged (ANT:ANT-VENUS ant AP1:AP1-GR ap1 cal) and untagged (AP1:AP1-GR ap1 cal) lines were collected five days after dex treatment when flowers are in stage 6/7 of development.

### About this project

This project is a collaboration between the Krizek lab at University of South Carolina and the Loraine Lab at UNC Charlotte.

A grant from the US National Science Foundation to PI Krizek and Co-PI Ann Loraine funded this work.

### About this repository

This repository contains analysis folders named for the type of data analysis code and results they contain.

Most analysis folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files that contain an analysis focused on answering one or two discrete questions. These are typically formatted like miniature research articles, with Introduction, Results/Analysis, and Conclusions sections.
* .html files - output from running knitr on .Rmd files.
*  results folders - contain files (typically tab-delimited) and images produced by .Rmd files.
*  data - data files obtained from a compute cluster or other external source upstream of this data analysis
*  src - R, python, or perl scripts used for simple data processing tasks
*  README.md - documentation

Code used for analysis, making figures, and making data files for visualization in external sites and programs are grouped into folders with names indicating their common purpose or analysis type.

Each module folder is designed to be run as a (mostly) self-contained project in RStudio. As such, each folder contains an ".Rproj" file. To run the code in RStudio, just open the .Rproj file and go from there. However, note that the code expects Unix-style file paths and depends on external libraries, mainly from Bioconductor.

Some modules depend on the output of other modules. Some modules also depend on externally supplied data files, which are version-controlled in ExternalDataSets but may also be available from external sites.
Questions?

### Contact:

* Ann Loraine aloraine@uncc.edu
* Beth Krizek krizek@sc.edu
* Nowlan Freese nfreese@uncc.edu
