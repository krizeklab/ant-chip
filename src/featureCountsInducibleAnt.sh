#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=6gb
#PBS -l walltime=8:00:00
#PBS -N inducible_ANT
cd $PBS_O_WORKDIR
module load subread
SAF=TAIR10_SAF_for_featureCount.txt 
BASE=inducible_ANT
OUT=$BASE.txt
ERR=$BASE.err
OUT2=$BASE.out
featureCounts -O -C -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
gzip $OUT

