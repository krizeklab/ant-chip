#!/usr/bin/env python

# to run this, get git repository https://bitbucket.org/lorainelab/igbquickload
# put igbquickload root directory in your PYTHONPATH

from Quickload import *
import QuickloadUtils as utils
from AnnotsXmlForRNASeq import *
import optparse

ANNOTSBACKGROUND='FFFFFF'

genome="A_thaliana_Jun_2009"
URL="https://krizek-lab.s3.amazonaws.com/"

# inducible ANT-GR RNA-Seq experiment
# ANT-GR colors
t1_t_fg="E61DDF"
t2_t_fg="AA1AF3"
t3_t_fg="4700FF"
t1_c_fg="4BBB85"
t2_c_fg="059F08"
t3_c_fg="006600"

# ANT-GR merged BAM file names
t1_t="ANT-induced-2h"
t2_t="ANT-induced-4h"
t3_t="ANT-induced-8h"
t1_c="ANT-mock-2h"
t2_c="ANT-mock-4h"
t3_c="ANT-mock-8h"

# s6 s7 ANT ChIP-seq - presented in Jan 2020 manuscript titled
# The Arabidopsis transcription factor AINTEGUMENTA orchestrates patterning genes and auxin signaling in the establishment of floral growth and form
ant_gr_title="ANT-GR RNA-Seq"
ant_gr_deploy_dir=URL+"ANT-GR_rnaseq/ANT-GR-rnaseq"
ant_gr_u=genome
ant_gr_lsts =[[t1_t,'ANT induced 2 hours',t1_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    [t1_c,'mock 2 hours',t1_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    [t2_t,'ANT induced 4 hours',t2_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    [t2_c,'mock 4 hours',t2_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    [t3_t,'ANT induced 8 hours',t3_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    [t3_c,'mock 8 hours',t3_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1dex2hours','reps/flat1 ANT induced 2 hours',t1_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2dex2hours','reps/flat2 ANT induced 2 hours',t1_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3dex2hours','reps/flat3 ANT induced 2 hours',t1_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4dex2hours','reps/flat4 ANT induced 2 hours',t1_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1mock2hours','reps/flat1 mock 2 hours',t1_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2mock2hours','reps/flat2 mock 2 hours',t1_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3mock2hours','reps/flat3 mock 2 hours',t1_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4mock2hours','reps/flat4 mock 2 hours',t1_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1dex4hours','reps/flat1 ANT induced 4 hours',t2_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2dex4hours','reps/flat2 ANT induced 4 hours',t2_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3dex4hours','reps/flat3 ANT induced 4 hours',t2_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4dex4hours','reps/flat4 ANT induced 4 hours',t2_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1mock4hours','reps/flat1 mock 4 hours',t2_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2mock4hours','reps/flat2 mock 4 hours',t2_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3mock4hours','reps/flat3 mock 4 hours',t2_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4mock4hours','reps/flat4 mock 4 hours',t2_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1dex8hours','reps/flat1 ANT induced 8 hours',t3_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2dex8hours','reps/flat2 ANT induced 8 hours',t3_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3dex8hours','reps/flat3 ANT induced 8 hours',t3_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4dex8hours','reps/flat4 ANT induced 8 hours',t3_t_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat1mock8hours','reps/flat1 mock 8 hours',t3_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat2mock8hours','reps/flat2 mock 8 hours',t3_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat3mock8hours','reps/flat3 mock 8 hours',t3_c_fg,ANNOTSBACKGROUND,ant_gr_u],
    ['flat4mock8hours','reps/flat4 mock 8 hours',t3_c_fg,ANNOTSBACKGROUND,ant_gr_u]]

# RNA-Seq for ChIP-Seq merged BAM names
s6s7_ANT="s6s7-rna-ANT_VENUS"
s3_ANT="s3-rna-ANT_VENUS"
s3_AIL6="s3-rna-AIL6_VENUS"
s6s7_ANT_fg="4169E1"
s3_ANT_fg="d9603b"
s3_AIL6_fg="be961e"

rna4chip_deploy_dir=URL+"RNA-Seq-from-ChiP-genotypes/rna-seq4chip-seq"
rna4chip_u="%s/%s"%(genome,rna4chip_deploy_dir)
rna4chip_title="RNA-seq for ChIP-Seq"
rnaseq4chipseq_lsts = [[s3_ANT,"Stage 3 ANT-VENUS",s3_ANT_fg,ANNOTSBACKGROUND,genome],
                       [s3_AIL6,"Stage 3 AIL6-VENUS",s3_AIL6_fg,ANNOTSBACKGROUND,genome],
                       [s6s7_ANT,"Stage 6,7 ANT-VENUS",s6s7_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B6","rep/B6 Stage 3 ANT-VENUS",s3_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B7","rep/B7 Stage 3 ANT-VENUS",s3_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B8","rep/B8 Stage 3 ANT-VENUS",s3_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B9","rep/B9 Stage 3 AIL6-VENUS",s3_AIL6_fg,ANNOTSBACKGROUND,genome],
                       ["B10","rep/B10 Stage 3 AIL6-VENUS",s3_AIL6_fg,ANNOTSBACKGROUND,genome],
                       ["B11","rep/B11 Stage 3 AIL6-VENUS",s3_AIL6_fg,ANNOTSBACKGROUND,genome],
                       ["B2","rep/B2 Stage 6,7 ANT-VENUS",s6s7_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B4","rep/B4 Stage 6,7 ANT-VENUS",s6s7_ANT_fg,ANNOTSBACKGROUND,genome],
                       ["B5","rep/B5 Stage 6,7 ANT-VENUS",s6s7_ANT_fg,ANNOTSBACKGROUND,genome]]

# s3 ANT AIL6 ChIP-seq
s3_deploy_dir=URL+"stage_3_chipseq/s3-chipseq"
s3_title="Stage 3 ChIP-Seq"
s3_txs_lsts=[
             ["s3-input-notag","s3 No tag - Input","c1cdcd"],
             ["s3-input-AIL6_VENUS","s3 AIL6-VENUS - Input","838b3b"],
             ["s3-input-ANT_VENUS","s3 A3 ANT-VENUS - Input","838b3b"],
             ["s3-chip-notag","s3 A4 No tag - ChIP","7ac5cd"],
             ["s3-chip-AIL6_VENUS","s3 A5 AIL6-VENUS - ChIP",s3_AIL6_fg],
             ["s3-chip-ANT_VENUS","s3 A6 ANT-VENUS - ChIP",s3_ANT_fg],
             ["A1","reps/s3 A1 No tag - Input","c1cdcd"],
             ["A2","reps/s3 A2 AIL6-VENUS - Input","838b3b"],
             ["A3","reps/s3 A3 ANT-VENUS - Input","838b3b"],
             ["A4","reps/s3 A4 No tag - ChIP","7ac5cd"],
             ["A5","reps/s3 A5 AIL6-VENUS - ChIP",s3_AIL6_fg],
             ["A6","reps/s3 A6 ANT-VENUS - ChIP",s3_ANT_fg],
             ["A7","reps/s3 A7 No tag - Input","c1cdcd"],
             ["A8","reps/s3 A8 AIL6-VENUS - Input","838b3b"],
             ["A9","reps/s3 A9 ANT-VENUS - Input","838b3b"],
             ["A10","reps/s3 A10 No tag - ChIP","7ac5cd"],
             ["A11","reps/s3 A11 AIL6-VENUS - ChIP",s3_AIL6_fg],
             ["A12","reps/s3 A12 ANT-VENUS - ChIP",s3_ANT_fg]]

# s6 s7 ANT ChIP-seq - presented in Jan 2020 manuscript titled
# The Arabidopsis transcription factor AINTEGUMENTA orchestrates patterning genes and auxin signaling in the establishment of floral growth and form
s6s7_deploy_dir=URL+"stage_6and7_chipseq/s6s7-chipseq-ANT_VENUS"
s6s7_title="Stage 6,7 ANT ChIP-Seq"
s6s7_lsts=[ ["s6s7-input-notag","s6,7 No tag - Input","c1cdcd"],
            ["s6s7-input-ANT_VENUS","s6,7 ANT-VENUS - Input","838b3b"],
            ["s6s7-chip-notag","s6,7 No tag - ChIP","7ac5cd"],
            ["s6s7-chip-ANT_VENUS","s6,7 ANT-VENUS - ChIP",s6s7_ANT_fg],
             ["K1","reps/s6,7 K1 No tag - Input","c1cdcd"],
             ["K2","reps/s6,7 K2 ANT-VENUS - Input","838b3b"],
             ["K3","reps/s6,7 K3 No tag - ChIP","7ac5cd"],
             ["K4","reps/s6,7 K4 ANT-VENUS - ChIP",s6s7_ANT_fg],
             ["K5","reps/s6,7 K5 No tag - Input","c1cdcd"],
             ["K6","reps/s6,7 K6 ANT-VENUS - Input","838b3b"],
             ["K7","reps/s6,7 K7 No tag - ChIP","7ac5cd"],
             ["K8","reps/s6,7 K8 ANT-VENUS - ChIP",s6s7_ANT_fg]]

# make Quickload annots.xml for Jan 2020 manuscript titled:
#   The Arabidopsis transcription factor AINTEGUMENTA orchestrates patterning genes
#   and auxin signaling in the establishment of floral growth and form
def makeQuickloadFilesForJan2020Manuscript():
    quickload_files=[]
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=ant_gr_lsts,folder=ant_gr_title,
                    deploy_dir=ant_gr_deploy_dir,include_scaled_bw=True,include_TH=False)
    for file in rnaseq_files:
        quickload_files.append(file)
    for lst in s6s7_lsts:
        f=lst[0]
        sample=lst[1]
        foreground_color=lst[2]
        descr = "reads aligned using bowtie2"
        fn = s6s7_deploy_dir + "/" + f + '.bam'
        title = s6s7_title + "/Reads/" + sample + " alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=genome,
                                 foreground_color=foreground_color,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)
        descr="scaled coverage"
        fn = s6s7_deploy_dir + "/" + f + ".chipseq.bw"
        title = s6s7_title + "/Graphs/" + sample + " scaled coverage"
        quickload_file=QuickloadFile(path=fn,track_name=title,track_info_url=genome,
                                    foreground_color=foreground_color,
                                    tool_tip=descr)
        quickload_files.append(quickload_file)
    return quickload_files

# make Quickload annots.xml for all data sets
def makeQuickloadFiles():
    quickload_files=[]
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=ant_gr_lsts,folder=ant_gr_title,
                    deploy_dir=ant_gr_deploy_dir,include_scaled_bw=True,include_TH=False)
    for file in rnaseq_files:
        quickload_files.append(file)
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=rnaseq4chipseq_lsts,folder=rna4chip_title,
                    deploy_dir=rna4chip_deploy_dir,include_scaled_bw_by_strand=True,
                    include_TH=False)
    for file in rnaseq_files:
        quickload_files.append(file)
    for lst in s3_txs_lsts:
        f=lst[0]
        sample=lst[1]
        foreground_color=lst[2]
        descr = "reads aligned using bowtie2"
        fn = s3_deploy_dir + "/" + f + '.bam'
        title = s3_title + "/Reads/" + sample + " alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=genome,
                                 foreground_color=foreground_color,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)
        descr="scaled coverage"
        fn = s3_deploy_dir + "/" + f + ".chipseq.bw"
        title = s3_title + "/Graphs/" + sample + " scaled coverage"
        quickload_file=QuickloadFile(path=fn,track_name=title,track_info_url=genome,
                                    foreground_color=foreground_color,
                                    tool_tip=descr)
        quickload_files.append(quickload_file)
    macs_deploy_dir=URL+"stage_3_chipseq/macs2-s3-chipseq-AIL6_VENUS"
    prefix="AIL6"
    igb_path=s3_title+"/macs2/AIL6"
    # http://krizek-lab.s3.amazonaws.com//stage_3_chipseq/macs2-s3-chipseq-AIL6_VENUS/AIL6_peaks.narrowPeak.g
    macs_files = makeMacs2Files(deploy_dir=macs_deploy_dir,
                                prefix=prefix,track_info_url=genome,
                                igb_path=igb_path,
                                exp_name="stage 3 AIL6")
    for file in macs_files:
        quickload_files.append(file)
        file.setForegroundColor(s3_AIL6_fg)
    macs_deploy_dir=URL+"stage_3_chipseq/macs2-s3-chipseq-ANT_VENUS"
    prefix="ANT"
    igb_path=s3_title+"/macs2/ANT"
    macs_files=makeMacs2Files(deploy_dir=macs_deploy_dir,
                              prefix=prefix,track_info_url=genome,
                              igb_path=igb_path,
                              exp_name="stage 3 ANT")
    for file in macs_files:
        quickload_files.append(file)
        file.setForegroundColor(s3_ANT_fg)
    for lst in s6s7_lsts:
        f=lst[0]
        sample=lst[1]
        foreground_color=lst[2]
        descr = "reads aligned using bowtie2"
        fn = s6s7_deploy_dir + "/" + f + '.bam'
        title = s6s7_title + "/Reads/" + sample + " alignments"
        quickload_file = BamFile(path=fn,track_name=title,track_info_url=genome,
                                 foreground_color=foreground_color,
                                 tool_tip=descr)
        quickload_files.append(quickload_file)
        descr="scaled coverage"
        fn = s6s7_deploy_dir + "/" + f + ".chipseq.bw"
        title = s6s7_title + "/Graphs/" + sample + " scaled coverage"
        quickload_file=QuickloadFile(path=fn,track_name=title,track_info_url=genome,
                                    foreground_color=foreground_color,
                                    tool_tip=descr)
        quickload_files.append(quickload_file)
    macs_deploy_dir=URL+"stage_6and7_chipseq/macs2-s6s7-chipseq-ANT_VENUS"
    prefix="48v37"
    igb_path=s6s7_title+"/macs2/ANT"
    macs_files=makeMacs2Files(deploy_dir=macs_deploy_dir,
                              prefix=prefix,track_info_url=genome,
                              igb_path=igb_path,
                              exp_name="stage 6,7 ANT")
    for file in macs_files:
        quickload_files.append(file)
        file.setForegroundColor(s6s7_ANT_fg)
    return quickload_files

def makeMacs2Files(deploy_dir=None,prefix=None,
                   igb_path=None,exp_name=None,
                   track_info_url=None):
    quickload_files = []
    fn=deploy_dir+"/"+prefix+"_peaks.narrowPeak.gz"
    title=igb_path+"/"+exp_name+" narrowPeak"
    descr="macs2 narrowPeak file"
    quickload_file=BindingSite(path=fn,
                               track_name=title,
                               tool_tip=descr,
                               track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    fn=deploy_dir+"/"+prefix+"_summits.bed.gz"
    title=igb_path+"/"+exp_name+" summits"
    descr="macs2 summits file"
    quickload_file=BindingSite(path=fn,
                               track_name=title,
                               tool_tip=descr,
                               track_info_url=track_info_url)
    quickload_files.append(quickload_file)
    return quickload_files


if __name__=='__main__':
    about="Write annots.xml text for Krizek Lab IGB Quickload site(s) to stdout"
    parser=optparse.OptionParser(about)
    parser.add_option("-m","--manuscriptJan2020",dest="jan2020",action="store_true",
                          help="Only include files relevant to Jan 2020 manuscript")
    (options,args)=parser.parse_args()
    if options.jan2020:
        quickload_files=makeQuickloadFilesForJan2020Manuscript()
    else:
        quickload_files=makeQuickloadFiles()
    txt = makeAnnotsXml(quickload_files=quickload_files)
    sys.stdout.write(txt)
