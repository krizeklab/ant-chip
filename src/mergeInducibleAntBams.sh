#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=24:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load samtools

samtools merge ANT-induced-2h.bam *dex2hours.bam

samtools merge ANT-mock-2h.bam *mock2hours.bam

samtools merge ANT-induced-4h.bam *dex4hours.bam 

samtools merge ANT-mock-4h.bam *mock4hours.bam

samtools merge ANT-induced-8h.bam *dex8hours.bam 

samtools merge ANT-mock-8h.bam *mock8hours.bam


for f in ANT*; do
    samtools index $f
done

