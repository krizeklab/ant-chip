#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load samtools

samtools merge s3-input-notag.bam A1.bam A7.bam

samtools merge s3-input-AIL6_VENUS.bam A2.bam A8.bam

samtools merge s3-input-ANT_VENUS.bam A3.bam A9.bam

samtools merge s3-chip-notag.bam A4.bam A10.bam

samtools merge s3-chip-AIL6_VENUS.bam A5.bam A11.bam

samtools merge s3-chip-ANT_VENUS.bam A6.bam A12.bam

for f in s3-*.bam; do
    samtools index $f
done
