#!/bin/bash
#PBS -l nodes=1:ppn=2
#PBS -l mem=8gb
#PBS -l walltime=15:00:00
#PBS -q copperhead

cd $PBS_O_WORKDIR
module load samtools

samtools merge s6s7-input-notag.bam K1.bam K5.bam

samtools merge s6s7-input-ANT_VENUS.bam K2.bam K6.bam

samtools merge s6s7-chip-notag.bam K3.bam K7.bam

samtools merge s6s7-chip-ANT_VENUS.bam K4.bam K8.bam

for f in s6s7*.bam; do
    samtools index $f
done
