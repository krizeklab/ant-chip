#!/bin/bash

# to run: 
#    qsub-bowtie2.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 

SCRIPT=bowtie2.sh
G=A_thaliana_Jun_2009
O=bowtie2_alignments

if ! [ -d "$O" ]; then
    mkdir $O
fi

FS=$(ls *_R1_001.fastq.gz)
for F1 in $FS
do
    S=$(basename $F1 _R1_001.fastq.gz)
    F2=${S}_R2_001.fastq.gz
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G,F1=$F1,F2=$F2 $SCRIPT"
    $CMD
done


