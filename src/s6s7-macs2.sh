#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=32gb
#PBS -l walltime=4:00:00
#PBS -N 48v37

# see
# ANT ChIP / ExternalDataSets / description.txt
# in git repository

# run like this:
# qsub macs2.sh 1>job.out 2>job.err
cd $PBS_O_WORKDIR
N=48v37
G=119667750 # Arabidopsis
M=~/src/MACS/bin/macs2 
Q=0.01

# K4,K8 replicate IP tagged ANT
# K3,K7 replicate IP untagged ANT
# CMD="$M callpeak -f BAM -B -g $G -q $Q --keep-dup auto -t K4.bam K8.bam -c K3.bam K7.bam -n $N"
# $CMD 
# compressMacs2Files.sh

N=48v26
# K4,K8 replicate IP tagged ANT
# K2,K6 replicate input for IP tagged ANT
CMD="$M callpeak -f BAM -B -g $G -q $Q --keep-dup auto -t K4.bam K8.bam -c K2.bam K6.bam -n $N"
$CMD 
compressMacs2Files.sh
