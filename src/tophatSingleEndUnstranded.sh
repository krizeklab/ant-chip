#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load bowtie2
module load tophat
G=A_thaliana_Jun_2009
O=ANT-GR-rnaseq
if [ ! -d $O ]; then
    mkdir $O
fi
tophat -p 8 -I 5000 -o $O/$S $G $F

